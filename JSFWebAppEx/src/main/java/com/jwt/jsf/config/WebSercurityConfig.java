package com.jwt.jsf.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@EnableWebSecurity
public class WebSercurityConfig extends WebSecurityConfigurerAdapter  {

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity.authorizeRequests().antMatchers("java.faces.resource/**")
                .permitAll().anyRequest().authenticated();
        httpSecurity.formLogin().loginPage("/index.xhtml").failureUrl("/index.xhtml?error=true");
        httpSecurity.logout().logoutSuccessUrl("/index.xhtml");
        httpSecurity.csrf().disable();
    }
    @Autowired
    public void ConfiguredGlobal(AuthenticationManagerBuilder auth) throws Exception{
        auth.inMemoryAuthentication().withUser("admin").password("admin").roles("USER").and()
                .withUser("admin").password("admin").roles("ADMIN");
    }

}
