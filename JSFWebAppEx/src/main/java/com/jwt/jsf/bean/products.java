package com.jwt.jsf.bean;

import org.primefaces.PrimeFaces;
import org.primefaces.event.CloseEvent;
import org.primefaces.event.MoveEvent;
import org.primefaces.model.LazyDataModel;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import java.io.IOException;
import java.io.Serializable;
import java.sql.*;
import java.util.ArrayList;
import java.util.Map;


//@Entity
//@Table(name = "products")
@ManagedBean(name = "products")
@RequestScoped
public class products implements Serializable {
    private Statement statement;
    private Connection con;
    private ResultSet rs;
    private PreparedStatement stm;
    ArrayList productList;

    private Map<String,Object> sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();

    private int id;
    private String code;
    private String name;
    private String price;
    private String is_delete;
    private Date create_date;


    public String getIs_delete() {
        return is_delete;
    }
    public void setIs_delete(String is_delete) {
        this.is_delete = is_delete;
    }
    public  products(int id,String code, String name, String price, String is_delete,Date create_date) {
        this.id = id;
        this.create_date= create_date;
        this.is_delete= is_delete;
        this.code = code;
        this.name = name;
        this.price = price;
    }

    public Date getCreate_date(Date dateLog) {
        return create_date;
    }

    public void setCreate_date(Date create_date) {
        this.create_date = create_date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public products(){}
    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getPrice() {
        return price;
    }
    public void setPrice(String price) {
        this.price = price;
    }


    /**
     * ThinhNV
     * @return
     * Connection mysql-config-1
     */
    public Connection getConnecttion(){
        try{
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection( "jdbc:mysql://localhost:3306/product","root", "root");
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return con;
    }

    /**
     * ThinhNV
     * @return
     * Lấy toàn bộ dữ liệu từ db
     */
//    public void DataPaginator(){
//        this.setRowCount(DataSer);
//    }
    public ArrayList getAllProduct(){
        ArrayList pr = new ArrayList();
        try {
            statement = getConnecttion().createStatement();
            rs = statement.executeQuery("select * from products where is_delete=1");
            while (rs.next()){
                products prs = new products();
                prs.setId(rs.getInt("id"));
                prs.setCode(rs.getString("code"));
                prs.setName(rs.getString("name"));
                prs.setPrice(rs.getString("price"));
                prs.setIs_delete(rs.getString("is_delete"));
                pr.add(prs);
            }
            con.close();
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return pr;
    }

    /**
     * ThinhNV
     * @throws IOException
     * Create new product.
     */
    public void CreateNewProduct() throws IOException {
        FacesMessage mess= null;
        boolean insert = false;
        FacesContext context = FacesContext.getCurrentInstance();
        int result =0;
        try{
            long millis =System.currentTimeMillis();
            Date dateLog =new Date(millis);
           // Timestamp date = new Timestamp(millis);
            con= getConnecttion();
            PreparedStatement stm = con.prepareStatement("insert into products(code,name,price,is_delete,date_create) value (?,?,?,?,?)");
            /*stm.setInt(1,id);*/
            stm.setString(1,code);
            stm.setString(2,name);
            stm.setString(3,price);
            stm.setString(4,"1");
            stm.setDate(5,dateLog);
            result = stm.executeUpdate();
            if (result !=0 && code !=null && name !=null && price !=null && price !="0" && price !=""){
                insert=true;
                mess= new FacesMessage(FacesMessage.SEVERITY_INFO,"success",code);
                context.getExternalContext().redirect("welcome.xhtml");
                // return "welcome.xhtml?faces-redirect=true";
            }else {
                insert=false;
                mess = new FacesMessage(FacesMessage.SEVERITY_WARN,"Các trường dữ liệu chưa được nhập",code);
                //return "insert.xhtml?faces-redirect=true";
            }
            FacesContext.getCurrentInstance().addMessage(null, mess);
            PrimeFaces.current().ajax().addCallbackParam("insert", insert);
                con.close();
         }catch (Exception ex){
            ex.printStackTrace();
        }

    }

    /**
     * ThinhNV
     * @param pr
     * Cập nhật sản phẩm
     */
    public void UpdateProducts(products pr)  {
        FacesMessage message = null;
        FacesContext context = FacesContext.getCurrentInstance();
        boolean edit = false;
        try{
            long millis =System.currentTimeMillis();
            Date dateLog =new Date(millis);
            int result = 0;
            con= getConnecttion();
            PreparedStatement stm= con.prepareStatement("update products set code=?,name=?,price=?,is_delete=? where id=?");
            stm.setString(1,pr.getCode());
            stm.setString(2,pr.getName());
            stm.setString(3,pr.getPrice());
            stm.setString(4,pr.getIs_delete());
            stm.setInt(5,pr.getId());
         //   stm.setDatepr.getCreate_date(dateLog));
           result =  stm.executeUpdate();
            if (result !=0 && pr.getCode() != null && pr.getName() !=null && pr.getPrice() !=null ){
                edit=true;
                message = new FacesMessage(FacesMessage.SEVERITY_INFO,"edit success",code);
                context.getExternalContext().redirect("welcome.xhtml");
            }else {
                edit=false;
                message = new FacesMessage(FacesMessage.SEVERITY_WARN,"edit false",code);
            }

            con.close();
        }catch (Exception ex){
            ex.printStackTrace();
        }
        FacesContext.getCurrentInstance().addMessage(null,message );
        PrimeFaces.current().ajax().addCallbackParam("edit", edit);
        //return "welcome.xhtml?faces-redirect=true";
    }

    /**
     * ThinhNV
     * @param id
     * @return
     * Xóa sản phẩm theo câu lệnh delete
     */
    public String DeleteProduct(int id  ){
        try{
            con= getConnecttion();
            PreparedStatement stm= con.prepareStatement("update products set is_delete=? where id="+id);
           // stm.setString(1,is_delete="0");
            stm.setString(1,"0");
            stm.executeUpdate();
            con.close();
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return "welcome.xhtml?faces-redirect=true";
    }

    /**
     * ThinhNV
     * @param id
     * @return
     * Lấy dữ liệu từ table theo ID
     */
    public String UpdateProduct(int id){
        products pr = null;
        System.out.println(id);
        try{
            con = getConnecttion();
            Statement stmt=getConnecttion().createStatement();
            ResultSet rs=stmt.executeQuery("select * from products where id = "+(id));
            rs.next();
            pr = new products();
            pr.setId(rs.getInt("id"));
            pr.setCode(rs.getString("code"));
            pr.setName(rs.getString("name"));
            pr.setPrice(rs.getString("price"));
            pr.setIs_delete(rs.getString("is_delete"));
            sessionMap.put("editProduct", pr);
            con.close();
        }catch(Exception e){
            System.out.println(e);
        }
        return "/edit.xhtml?faces-redirect=true";
    }

    /**
     * ThinhNV
     * Điều hướng phân trang multipage
     *
     */

    public String insertPage(){

        return "insert";

    }
    public String editPage(){
        return "edit";
    }
    public String welcomePage(){
        return "welcome";
    }

    /**
     * ThinhNV
     * @param event
     * Xây dựng dialog
     */
        public void handleClose(CloseEvent event) {
            addMessage(event.getComponent().getId() + " closed", "So you don't like nature?");
        }

        public void handleMove(MoveEvent event) {
            addMessage(event.getComponent().getId() + " moved", "Left: " + event.getLeft() + ", Top: " + event.getTop());
        }

        public void destroyWorld() {
            addMessage("System Error", "Please try again later.");
        }

        public void addMessage(String summary, String detail) {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, detail);
            FacesContext.getCurrentInstance().addMessage(null, message);
        }

    }


