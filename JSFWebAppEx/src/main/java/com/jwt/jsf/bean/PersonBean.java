package com.jwt.jsf.bean;

import org.primefaces.PrimeFaces;

import java.io.IOException;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

@ManagedBean(name="personBean")
@SessionScoped
public class PersonBean implements Serializable {

	private String username;
	private String password;
	private String user_role;
	private String dbUserName;
	private String dbPassWord;
	private Double active;
	Connection connection;
	Statement statement;
	ResultSet resultSet;
	String sql;

	public Double getActive() {
		return active;
	}

	public void setActive(Double active) {
		this.active = active;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUser_role() {
		return user_role;
	}

	public void setUser_role(String user_role) {
		this.user_role = user_role;
	}

	public String getDbUserName() {
		return dbUserName;
	}

	public void setDbUserName(String dbUserName) {
		this.dbUserName = dbUserName;
	}

	public String getDbPassWord() {
		return dbPassWord;
	}

	public void setDbPassWord(String dbPassWord) {
		this.dbPassWord = dbPassWord;
	}


	/**
	 * ThinhNV
	 * @param userName
	 * Connection mysql
	 */
	public void dbData(String userName)
	{
		try
		{
			Class.forName("com.mysql.jdbc.Driver");
			connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/product","root","root");
			statement = connection.createStatement();
			sql = "Select * from accounts where Username like ('" + username +"')";
			resultSet = statement.executeQuery(sql);
			resultSet.next();
			dbUserName = resultSet.getString(2).toString();
			dbPassWord = resultSet.getString(3).toString();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			System.out.println("loi dang nhap" + ex);
		}
	}

	/**
	 * ThinhNV
	 * @throws IOException
	 * Kiểm tra tính xác thực .
	 */
	public  void checkValidUser() throws IOException {
		FacesMessage mess = null;
		boolean loggined = false;
		FacesContext context = FacesContext.getCurrentInstance();
		dbData(username);
		if (username.equalsIgnoreCase(dbUserName) && username != null) {
			if (password.equals(dbPassWord) && password != null) {
				loggined = true;
				mess = new FacesMessage(FacesMessage.SEVERITY_INFO, "Welcome", username);
				context.getExternalContext().redirect("welcome.xhtml");
			} else {
				loggined = false;
				mess = new FacesMessage(FacesMessage.SEVERITY_WARN, "Tài khoản hoặc Mật khẩu của bạn chưa đúng!", username);
			}
			FacesContext.getCurrentInstance().addMessage(null, mess);
			PrimeFaces.current().ajax().addCallbackParam("loggedIn", loggined);
		}
	}
	}



